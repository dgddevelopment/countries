<?php
namespace Country\Facades;

use Illuminate\Support\Facades\Facade;

/**
* @see \Illuminate\Foundation\Application
*/
class Country extends Facade
{
    /**
    * Get the registered name of the component.
    *
    * @return string
    */
    protected static function getFacadeAccessor()
    {
        return 'country';
    }
}
