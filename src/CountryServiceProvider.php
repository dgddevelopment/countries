<?php
namespace Country;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class CountryServiceProvider extends ServiceProvider
{

    public function register()
    {
        App::bind('country', function() {
            return new Country();
        });
    }

    public function boot()
    {

        //publish migrations
        $migrationPath = __DIR__.'/migrations';
        $this->publishes([
            $migrationPath => base_path('database/migrations'),
        ], 'migrations');

        // publish seeds
        $seedPath = __DIR__.'/seeds';
        $this->publishes([
            $seedPath => base_path('database/seeds'),
        ], 'seeds');

        // publish models
        $modelPath = __DIR__.'/Models';
        $this->publishes([
            $modelPath => base_path('app/Models'),
        ], 'models');

    }

}
