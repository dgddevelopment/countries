<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $appends = ['flag'];

    public function states(){
        return $this->hasMany(State::class, 'region_id');
    }

    public function getFlagAttribute(){
        return '<span class="flag-icon flag-icon-'.strtolower($this->iso).'"></span>';
    }
}
