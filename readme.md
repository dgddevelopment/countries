## Installation

- Add "address/countries":"master@dev"to composer.json require-dev section
- Add  to composer json following lines  
           "repositories": [
                    {
                        "url": "https://dg-datenschutz@bitbucket.org/dgddevelopment/countries.git",
                        "type": "git"
                    }
                ]
    
- Run composer update

This will install package with all his dependencies


#Configuration

- Add Country\CountryServiceProvider::class in config/app.php providers section
- Run php artisan vendor:publish --provider="Country\CountryServiceProvider" --tag=migrations to publish migration file
- Run php artisan vendor:publish --provider="Country\CountryServiceProvider" --tag=seeds to publish seeds
- Run php artisan vendor:publish --provider="Country\CountryServiceProvider" --tag=models to publish models
- Run php artisan migrate
- Add seed files into "DatabaseSeeder" file
- Run php artisan db:seed
- npm install --save-dev flag-icon-css
- Import '~flag-icon-css/sass/flag-icon.scss';
